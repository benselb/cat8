#include <SFML/Graphics.hpp>
#include <iostream>
#include <ctime>
#include "vm.h"

#define FRAMES_PER_SECOND      60
#define INSTRUCTIONS_PER_FRAME 4

using namespace std;

sf::Key::Code keymap[16] =
{ sf::Key::X, sf::Key::Num1, sf::Key::Num2, sf::Key::Num3, sf::Key::Q,
		sf::Key::W, sf::Key::E, sf::Key::A, sf::Key::S, sf::Key::D, sf::Key::Z,
		sf::Key::C, sf::Key::Num4, sf::Key::R, sf::Key::F, sf::Key::V };

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		cerr << "Usage: " << argv[0] << " filename" << endl;
		return 1;
	}

	int width = 800, height = 400;
	float px_w = ((float)width) / 64;
	float px_h = ((float)height) / 32;

	srand(time(NULL));

	// Create main window
	sf::RenderWindow App(sf::VideoMode(width, height), "cat8");

	VirtualMachine vm;
	if (vm.load_file(argv[1]))
	{
		cerr << "Couldn't load game." << endl;
		return 1;
	}

	bool keys[16];
	memset(keys, 0, sizeof(bool) * 16);

	sf::Clock clock;

	App.SetFramerateLimit(FRAMES_PER_SECOND);
	while (App.IsOpened())
	{
		/* Process events */
		sf::Event Event;
		while (App.GetEvent(Event))
		{
			if (Event.Type == sf::Event::Closed)
				App.Close();
		}

		for (int i = 0; i < 16; ++i)
		{
			keys[i] = App.GetInput().IsKeyDown(keymap[i]);
		}

		for (int i = 0; i < INSTRUCTIONS_PER_FRAME; ++i)
		{
			vm.step(clock.GetElapsedTime(), keys);
			clock.Reset();
		}

		if (vm.get_error())
		{
			cerr << vm.get_error() << endl;
			return 1;
		}

		App.Clear();
		sf::Color color = (vm.tone() ? sf::Color::Red : sf::Color::Yellow);
		bool *display = vm.get_display();
		for (int y = 0; y < 32; ++y)
		{
			for (int x = 0; x < 64; ++x)
			{
				if (display[y * 64 + x])
				{
					App.Draw(sf::Shape::Rectangle(x * px_w, y * px_h, x * px_w
							+ px_w, y * px_h + px_h, color));
				}
			}
		}
		App.Display();
	}

	return 0;
}
