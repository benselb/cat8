#include "vm.h"

#include "font.h"

//#define dbgprintf(...) fprintf(stderr, __VA_ARGS__)
#define dbgprintf(...)

VirtualMachine::VirtualMachine(void)
{
	memset(registers, 0, 16);
	memset(mem, 0, CHIP8_MEMSIZE);
	for (int ch = 0; ch < 16; ++ch)
	{
		for (int row = 0; row < 5; ++row)
		{
			int byte = 0;
			for (int bit = 0; bit < 4; ++bit)
			{
				int set = (font[ch*5*4+row*4+bit] == ' ' ? 0 : 1);
				byte = (byte << 1) | set;
			}
			byte <<= 4;
			mem[ch*5+row] = byte;
		}
	}

#ifdef USE_BOOT_SCREEN
	for (int y = 0; y < 32; ++y)
	{
		for (int x = 0; x < 64; ++x)
		{
			display[y*64+x] = (boot_screen[y*64+x] != ' ');
		}
	}
#else
	memset(display, 0, 64*32);
#endif

	ip = 0x200;
	tick_time = 0;
	sound_timer = 0;
	delay_timer = 0;
	error = NULL;
}

int VirtualMachine::load_file(const char *fname)
{
	FILE *f = fopen(fname, "rb");
	if (!f) return 1;
	fread(mem+0x200, 1, CHIP8_EFFECTIVE_MEMSIZE, f);
	fclose(f);
	return 0;
}

bool VirtualMachine::tone(void)
{
	return (sound_timer > 0);
}

void VirtualMachine::fail(const char *msg)
{
	error = msg;
}

const char * VirtualMachine::get_error(void)
{
	return error;
}

bool * VirtualMachine::get_display(void)
{
	return display;
}

void VirtualMachine::step(double dt, const bool *keys)
{
	if (error)
		return;

	tick_time += dt;
	while (tick_time > (1.0 / 60.0))
	{
		tick_time -= (1.0 / 60.0);
		if (delay_timer > 0) --delay_timer;
		if (sound_timer > 0) --sound_timer;
	}

	if (ip > (CHIP8_MEMSIZE - 2))
	{
		fail("Instruction pointer out of bounds");
		return;
	}

	uint8_t msb = mem[ip];
	uint8_t lsb = mem[ip + 1];
	ip += 2;

	dbgprintf("ip: %d(%x)   instr: %02x%02x\n", ip-2, ip-2, msb, lsb);

	/* See http://en.wikipedia.org/wiki/Chip8 for background to these
	 * names. The switch statement is a very literal translation of the table
	 * of opcodes there.
	 */
#define X 	(msb & 0xF)
#define Y 	(lsb >> 4)
#define VX	registers[X]
#define VY	registers[Y]
#define VF	registers[0xF]
#define NNN	((X << 8) | lsb)
#define NN 	(lsb)
#define N 	(lsb & 0xF)

	int result, ones, tens, hundreds, x, y;
	switch (msb >> 4)
	{
	case 0:
		if (msb == 0x00 && (lsb >> 4) == 0xE)
		{
			if ((lsb & 0xF) == 0)
			{
				memset(display, 0, 64 * 32);
				return;
			}
			else if ((lsb & 0xF) == 0xE)
			{
				if (return_stack.empty())
				{
					fail("Stack underflow");
					return;
				}
				ip = return_stack.top();
				return_stack.pop();
				return;
			}
		}
		fail("Game requires unimplemented RCA 1802 emulation");
		break;

	case 2:
		return_stack.push(ip);
		/* fall through on purpose */
	case 1:
		ip = NNN;
		break;

	case 3:
		if (VX == NN)
			ip += 2;
		break;

	case 4:
		if (VX != NN)
			ip += 2;
		break;

	case 5:
		if (VX == VY)
			ip += 2;
		break;

	case 6:
		VX = NN;
		break;

	case 7:
		VX += NN;
		break;

	case 8:
		switch (N)
		{
		case 0:
			VX = VY;
			break;
		case 1:
			VX = VX | VY;
			break;
		case 2:
			VX = VX & VY;
			break;
		case 3:
			VX = VX ^ VY;
			break;
		case 4:
			result = (int)VX + (int)VY;
			VX = result;
			VF = (result > 255) ? 1 : 0;
			break;
		case 5:
			result = VX - VY;
			VF = (VY > VX) ? 0 : 1;
			VX = result;
			break;
		case 6:
			VF = VX & 0x1;
			VX >>= 1;
			break;
		case 7:
			result = VY - VX;
			VF = (VX > VY) ? 0 : 1;
			VX = result;
			break;
		case 0xE:
			VF = VX & (1 << 7);
			VX <<= 1;
			break;
		}
		break;

	case 9:
		if (VX != VY)
			ip += 2;
		break;

	case 0xA:
		index = NNN;
		break;

	case 0xB:
		ip = NNN + registers[0];
		break;

	case 0xC:
		VX = rand() & NN;
		break;

	case 0xD:
		VF = 0;
		y = VY % 32;
		for (int row = 0; row < N; ++row)
		{
			if (y > 31)
				y = 0;
			int addr = index + row;
			if (addr > CHIP8_MEMSIZE - 1)
			{
				fail("Tried to draw a sprite from out-of-bounds memory.");
				return;
			}
			uint8_t rowdata = mem[addr];
			x = VX % 64;
			for (int i = 0; i < 8; ++i)
			{
				if (x > 63)
					x = 0;

				bool flip = (rowdata & (1 << 7));
				bool & pixel = display[64 * y + x];
				if (flip && pixel)
					VF = 1;
				if (flip)
					pixel = !pixel;

				rowdata <<= 1;
				++x;
			}
			++y;
		}
		break;

	case 0xE:
		switch (lsb)
		{
		case 0x9E:
			if (keys[VX%16])
				ip += 2;
			break;
		case 0xA1:
			if (!keys[VX%16])
				ip += 2;
			break;
		default:
			fail("Invalid opcode EX??");
			break;
		}
		break;

	case 0xF:
		switch(lsb)
		{
		case 0x07:
			VX = delay_timer;
			break;
		case 0x0A:
			for (int i = 0; i < 16; ++i)
			{
				if (keys[i])
				{
					VX = i;
					return;
				}
			}
			/* Maybe this should be made better than a busy loop, but it's not
			 * used commonly enough in actual games to bother.
			 */
			ip -= 2;
			break;
		case 0x15:
			delay_timer = VX;
			break;
		case 0x18:
			sound_timer = VX;
			break;
		case 0x1E:
			index += VX;
			break;
		case 0x29: /* Locate character in font */
			index = VX*5;
			break;
		case 0x33:
			if (index + 2 > CHIP8_MEMSIZE - 1)
			{
				fail("Out of bounds in BCD operation");
				return;
			}
			hundreds = VX / 100;
			tens = (VX - hundreds*100) / 10;
			ones = (VX - hundreds*100 - tens*10);
			mem[index] = hundreds;
			mem[index + 1] = tens;
			mem[index + 2] = ones;
			break;
		case 0x55:
			if (index + X > CHIP8_MEMSIZE - 1)
			{
				fail("Out of bounds saving registers");
			}
			for (int i = 0; i <= X; ++i)
			{
				mem[index+i] = registers[i];
			}
			break;
		case 0x65:
			if (index + X > CHIP8_MEMSIZE - 1)
			{
				fail("Out of bounds restoring registers");
			}
			for (int i = 0; i <= X; ++i)
			{
				registers[i] = mem[index+i];
			}
			break;
		}
	}
}
