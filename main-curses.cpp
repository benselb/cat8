#include <iostream>
#include <ctime>
#include <ncurses.h>
#include <unistd.h>
#include "vm.h"

#define FRAMES_PER_SECOND      20
#define INSTRUCTIONS_PER_FRAME 12

using namespace std;

const char keymap[17] = "x123qweasdzc4rfv";

void get_input(bool *keys, bool &running, int &font, int &speed)
{
	int c, nkeys = 0;
	while ((c = wgetch(stdscr)) != ERR && nkeys < 8)
	{
		++nkeys;
		if (c == '\n')
		{
			running = false;
		}
		else if (c == 'o')
		{
			--font;
		}
		else if (c == 'p')
		{
			++font;
		}
		else if (c == 'n')
		{
			speed -= 5;
			if (speed <= 0) speed = 5;
		}
		else if (c == 'm')
		{
			speed += 5;
			if (speed > 300) speed = 300;
		}
		else
		{
			for (int i = 0; i < 16; ++i)
			{
				if (c == keymap[i])
				{
					keys[i] = true;
				}
			}
		}
	}
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		cerr << "Usage: " << argv[0] << " filename" << endl;
		return 1;
	}

	srand(time(NULL));

	const char fonts[] = ":'." "|'." ":`," "###";
	const int nfonts = sizeof(fonts) / 3;
	int font = 0;
	int speed = 100;

	VirtualMachine vm;
	if (vm.load_file(argv[1]))
	{
		cerr << "Couldn't load game." << endl;
		return 1;
	}

	bool keys[16];

	initscr();
	raw();
	noecho();
	wtimeout(stdscr, 0);
	start_color();
	init_pair(1, COLOR_YELLOW, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	mvprintw(17, 0, "1 2 3 4    Enter: Quit");
	mvprintw(18, 0, "Q W E R      o/p: Change pixel font");
	mvprintw(19, 0, "A S D F      n/m: Change speed");
	mvprintw(20, 0, "Z X C V");

	bool running = true;
	while (running)
	{
		int fps = (FRAMES_PER_SECOND * speed) / 100;
		/* Execute instructions */
		memset(keys, 0, sizeof(bool) * 16);
		usleep(1000000 / fps);
		get_input(keys, running, font, speed);
		vm.step(1.0 / fps, keys);
		for (int i = 0; i < INSTRUCTIONS_PER_FRAME - 1; ++i)
		{
			get_input(keys, running, font, speed);
			vm.step(0, keys);
		}

		if (vm.get_error())
		{
			endwin();
			cerr << vm.get_error() << endl;
			return 1;
		}

		/* Display */
		bool *display = vm.get_display();
		attron(A_BOLD | COLOR_PAIR(vm.tone() ? 2 : 1));
		font = font % nfonts;
		const char *curfont = fonts + font*3;
		for (int y = 0; y < 16; ++y)
		{
			for (int x = 0; x < 64; ++x)
			{
				bool upper = display[y * 2 * 64 + x];
				bool lower = display[(y * 2 + 1) * 64 + x];
				int ch = (upper && lower ? curfont[0] : upper ? curfont[1] : lower ? curfont[2] : ' ');
				mvaddch(y, x, ch);
			}
		}

		refresh();
	}

	endwin();

	return 0;
}
