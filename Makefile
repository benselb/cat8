LDFLAGS=-lsfml-system -lsfml-window -lsfml-graphics
LDFLAGS_CURSES=-lncurses
CFLAGS=-O3 -Wall

all: cat8

curses: cat8-curses

cat8-curses: vm.o main-curses.o
	g++ -o cat8-curses vm.o main-curses.o $(LDFLAGS_CURSES)

cat8: vm.o main.o
	g++ -o cat8 vm.o main.o $(LDFLAGS) 

vm.o: vm.cpp vm.h font.h
	g++ $(CFLAGS) -o vm.o -c vm.cpp
	
main.o: main.cpp
	g++ $(CFLAGS) -o main.o -c main.cpp
		
main-curses.o: main-curses.cpp
	g++ $(CFLAGS) -o main-curses.o -c main-curses.cpp

clean:
	rm -f *.o cat8 cat8-curses
