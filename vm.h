#include <stdint.h>
#include <cstring>
#include <cstdlib>
#include <stack>
#include <cstdio>

#define CHIP8_MEMSIZE (1<<12)
#define CHIP8_EFFECTIVE_MEMSIZE (CHIP8_MEMSIZE - 512)

class VirtualMachine
{

public:
	VirtualMachine(void);

	/* Load a file into memory; returns 0 on success. */
	int load_file(const char *fname);

	/* Gets the contents of the display as a flat array of 64*32 bools */
	bool * get_display(void);

	/* Fetches and runs a single instruction.
	 * Arguments:
	 *    dt: The time in seconds since the last step
	 *    keys: An array of boolean values representing whether the given key
	 *          (0 to F) is pressed. The layout should be similar to:
	 *
	 *           1 2 3 C
	 *           4 5 6 D
	 *           7 8 9 E
	 *           A 0 B F
	 */
	void step(double dt, const bool *keys);

	/* Returns NULL if all is well, or an error string if a fatal VM error
	 * occurred.
	 */
	const char *get_error(void);

	/* Returns true if a tone should be playing. */
	bool tone(void);

private:
	uint8_t mem[CHIP8_MEMSIZE];
	uint8_t registers[16];
	uint8_t delay_timer;
	uint8_t sound_timer;
	uint16_t index;
	int ip;
	std::stack<uint16_t> return_stack;

	bool display[64*32];

	const char *error;

	double tick_time;

	void fail(const char *msg);
};
